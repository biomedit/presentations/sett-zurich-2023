<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
    />
    <title>🦀 sett</title>

    <meta
      name="description"
      content="sett: data encryption and transfer made easy(ier)"
    />
    <meta name="author" content="Christian Ribeaud, Jarosław Surkont" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta
      name="apple-mobile-web-app-status-bar-style"
      content="black-translucent"
    />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>reveal.js</title>

    <link rel="stylesheet" href="dist/reset.css" />
    <link rel="stylesheet" href="dist/reveal.css" />
    <link rel="stylesheet" href="dist/theme/white.css" />
    <link rel="stylesheet" href="css/custom.css" />

    <link rel="stylesheet" href="plugin/highlight/monokai.css" />
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
        <section>
          <h2 style="margin-bottom: 100px">
            sett: data encryption and transfer made easy(ier)
          </h2>
          <p style="margin-bottom: 50px">
            by Christian Ribeaud and Jarosław Surkont
          </p>
          <p>
            <a href="https://biomedit.gitlab.io/presentations/sett-zurich-2023"
              >biomedit.gitlab.io/presentations/sett-zurich-2023</a
            >
          </p>
        </section>

        <section>
          <h2>Outline</h2>
          <ul>
            <li><strong>BioMedIT</strong> mission 🧑‍🚀</li>
            <li>1st attempt (production): Python 🐍</li>
            <li>2nd attempt (in progress): Rust 🦀</li>
            <li>Maintenance 🛠️</li>
            <li>Future plans 🔮</li>
            <li>Summing up ⭐</li>
          </ul>
          <aside class="notes" data-markdown>
            - Say thank you for the invitation
          </aside>
        </section>

        <section>
          <section>
            <h2>Big picture</h2>
            <a href="https://www.biomedit.ch/home.html">
              <img src="img/biomedit.png" height="500px" />
            </a>
          </section>

          <section>
            <h3>
              <a href="https://gitlab.com/biomedit/sett/" target="_blank"
                >sett</a
              >
              - Characteristics
            </h3>
            <ul>
              <li>End-to-end encryption (<strong>OpenPGP</strong>)</li>
              <li>Trust: (meta)data signing (<strong>OpenPGP</strong>)</li>
              <li>Data compression (<strong>gzip</strong>)</li>
              <li>Data integrity validation (<strong>sha256</strong>)</li>
              <li>
                Packaging: a single, self-contained file (<strong>zip</strong>)
              </li>
              <li>Transfer (<strong>sftp</strong>, <strong>s3</strong>)</li>
              <li>Cross-platform support</li>
              <li><strong>CLI</strong> and <strong>GUI</strong></li>
              <li>Interoperability with other tools</li>
            </ul>

            <p class="fragment"><strong>UX: Keep it easy!</strong></p>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - **sett**: Secure Encryption and Transfer Tool
              - Public key encryption (asymmetric encryption). Encryption and decryption use different keys.
              - The **integrity** of a message being transferred is maintained when it is protected from unauthorized changes during transit.
              - In case of metadata, we have a detached (kept separate from its signed data) signature. Verification happens then with both files. 
              - Can be used outside the **BioMedIT** ecosystem which adds some constraints to the tool.
              - `Interoperability with other tools`: **sett** is a facilitator. You basically could perform all the steps using other tools.
              - **gzip** is a compression algorithm and a file format, **zip** (the *catalog* of the collection is separate from the
                collection itself) is a container. We used to use **tar** but streaming is more difficult to implement in case of **tar**
                (**tar** compression takes advantage of similarities among the files) files.
              </textarea>
            </aside>
          </section>

          <section>
            <h3>Package structure</h3>
            <div class="r-stack">
              <div class="fragment fade-in-then-out">
                <pre style="min-width: 40em"><code data-trim class="text">
                  YYYYMMDDThhmmss.zip
                  ├── metadata.json
                  ├── metadata.json.sig
                  └── data.tar.gz.gpg
                      └── data.tar.gz
                          ├── content/
                          |   ├── [file1]
                          |   ├── [file2]
                          |   └── ...
                          └── checksum.sha256
                </code></pre>
              </div>
              <div class="fragment fade-in-then-out">
                <h4>metadata.json</h4>
                <pre
                  style="min-width: 40em"
                ><code data-trim data-noescape class="json">
                  {
                      "transfer_id": 42,
                      "sender": "AAABFBC698539AB6CE60BDBE8220117C2F906548",
                      "recipients": ["D99AD936FC83C9BABDE7C33E1CF8C1A2076818C3"],
                      "timestamp": "2020-01-29T15:31:42+0100",
                      "checksum": "...36da5d98a1dc6dceee21d62f694d71c4cf184",
                      "checksum_algorithm": "SHA256",
                      "compression_algorithm": "gzip",
                      "purpose": "PRODUCTION",
                      "version": "0.7"
                  }
                </code></pre>
              </div>
              <div class="fragment fade-in-then-out">
                <h4>checksum.sha256</h4>
                <pre
                  style="min-width: 40em"
                ><code data-trim data-noescape class="text" style="min-height: 10em;">
                  ...d80a840a190ca997ad8044a67c4c1683f7b63 file1.csv
                  ...790a4f481bb49805e2d1f380df0c636792ff6 folder1/file.txt
                  ...6ef658502c9d0b05dd4a2185d0f94ccf165cf folder1/folder2/file.txt
                </code></pre>
              </div>
            </div>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - Checksum file of all files present in `data.tar.gz`. This is used to make sure
                nothing was corrupted during the encryption/transfer/decryption process.
              </textarea>
            </aside>
          </section>
        </section>

        <section>
          <section>
            <h2>Python 🐍</h2>
            <a href="https://xkcd.com/353/">
              <img src="img/python_xkcd.png" height="500px" />
            </a>
          </section>

          <section>
            <img src="img/sett_python.png" height="500px" />
          </section>

          <section>
            <h3>Support</h3>
            <ul>
              <li>
                <strong>Linux</strong> (including <strong>CentOS 7 🤯</strong> -
                <a href="https://endoflife.date/centos" target="_new"
                  >EOL 2024-06-30</a
                >), <strong>MacOS</strong>, <strong>Windows</strong>
              </li>
              <li><strong>Python</strong> 3.7-3.11</li>
              <li>
                <strong>GnuPG</strong> 2.0.22 (<a
                  href="https://gnupg.org/download/index.html#end-of-life"
                  target="_new"
                  >EOL 2017-12-31 💣</a
                >) - latest (with
                <a href="https://gitlab.com/biomedit/gpg-lite">gpg-lite)</a>
              </li>
            </ul>
          </section>

          <section>
            <h3>Lessons learned</h3>
            <ul>
              <li>
                <strong>Cross-platform</strong> support is difficult, especially
                if none of the developers uses a given platform.
              </li>
              <li>
                Relying on packages installed separately
                (<strong>Python</strong>, <strong>GnuPG</strong>,
                <strong>Qt</strong>, <strong>glibc</strong>) adds a significant
                development cost and you'd better master them!
              </li>
              <li>App installation/upgrade is challenging to end users.</li>
            </ul>
          </section>

          <section>
            <h3>What can we do? 🤷‍♀️</h3>
            <ol>
              <li class="fragment">
                Reduce the number of dependencies: <strong>GnuPG</strong> is the
                most problematic one!
              </li>
              <li class="fragment">
                Rewrite everything from scratch in <strong>JavaScript</strong>!
                Everything is a browser and the browser is the new <b>OS</b>.
              </li>
              <li class="fragment">
                Surgical replacement of specific workflows (encryption,
                transfer, decryption, <strong>OpenPGP</strong> key management)
                and/or user interfaces (<b>CLI</b>, <b>GUI</b>) in
                <strong>Rust</strong>.
              </li>
            </ol>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - https://openpgpjs.org/: **OpenPGP** JavaScript implementation
              - How did we come to the idea of using **Rust**?
                - **Geri**
                - **SequoiaPGP** release of version *1.0.0* on 2020-12-16
                - **Python** bindings
              </textarea>
            </aside>
          </section>
        </section>

        <section>
          <section>
            <h2>Let's rewrite it in 🦀!</h2>
            <a href="https://xkcd.com/2314/">
              <img src="img/carcinization.png" style="height: 300px" />
            </a>
          </section>

          <section>
            <h3>Reasons</h3>
            <ul>
              <li>Portability</li>
              <li>Security</li>
              <li>Correctness: capture bugs earlier</li>
              <li>Ecosystem</li>
              <ul>
                <li>SequoiaPGP</li>
                <li>PyO3</li>
              </ul>
            </ul>
          </section>

          <section>
            <h3>Challenges</h3>
            <ul>
              <li>
                Steep learning curve: you need an expert and a lot of
                motivation!
              </li>
              <li>
                In the transition period, both 🦀 and 🐍 codebases need
                maintenance.
              </li>
            </ul>
          </section>

          <section
            data-background-image="img/sequoia_home.png"
            data-background-size="contain"
            data-background-opacity="0.5"
          >
            <h3>SequoiaPGP</h3>
            <ul>
              <li>A new OpenPGP implementation (<b>GnuPG</b> replacement)</li>
              <li><b>v1.0.0</b> released on <em>16th December 2020</em></li>
              <li>Library-first approach</li>
              <li>
                Active
                <a
                  href="https://sequoia-pgp.org/community/"
                  target="_blank"
                  rel="noopener"
                  >community</a
                >, ready to help and attentive
              </li>
              <li>
                An ecosystem, e.g.
                <a href="https://gitlab.com/sequoia-pgp/sequoia-sq">sq</a>,
                <a href="https://keys.openpgp.org/">keys.openpgp.org</a>
              </li>
            </ul>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - Mention the story about this one maintainer getting in touch with us from nowhere to give
              us some advices on key store implementation.
              </textarea>
            </aside>
          </section>

          <section>
            <h3>Python bindings</h3>
            <p><a href="https://pyo3.rs/">PyO3</a></p>
            <blockquote cite="https://pyo3.rs/">
              Rust bindings for Python, including tools for creating native
              Python extension modules. Running and interacting with Python code
              from a Rust binary is also supported.
            </blockquote>
            <p><a href="https://github.com/PyO3/maturin">Maturin</a></p>
            <blockquote cite="https://github.com/PyO3/maturin">
              Build and publish crates with pyo3, rust-cpython, cffi and uniffi
              bindings as well as rust binaries as python packages.
            </blockquote>
          </section>

          <section>
            <h3>Repository Structure</h3>
            <ul>
              <li>
                <a
                  href="https://gitlab.com/biomedit/sett-rs/-/tree/main/sett"
                  target="_blank"
                  >sett</a
                >
                - the main lib+bin crate
              </li>
              <li>
                <a
                  href="https://gitlab.com/biomedit/sett-rs/-/tree/main/sett-rs"
                  target="_blank"
                  >sett-rs</a
                >
                - Python bindings to the main library
              </li>
            </ul>
          </section>

          <section>
            <pre><code data-trim data-noescape class="text"><script type="text/template">
              ❯ sett
              Rust port of sett (data compression, encryption and transfer tool).

              Usage: sett <COMMAND>

              Commands:
                encrypt    Encrypt data package
                decrypt    Decrypt data package
                transfer   Transfer data package
                certstore  PGP key management
                help       Print this message or the help of the given subcommand(s)

              Options:
                -h, --help     Print help
                -V, --version  Print version
            </script></code></pre>
          </section>

          <section>
            <h3>High-level API</h3>
            <pre
              style="min-width: 40em"
            ><code data-trim data-noescape class="rust"><script type="text/template">
            pub fn encrypt<P: AsRef<Path>, S: AsRef<str>, A: AsRef<[S]>, T: Encrypt>(
                opts: &EncryptOpts<P, S, A, impl Progress>,
                dest: &T,
            ) -> Result<Option<String>> {}

            pub trait Encrypt {
                fn encrypt<P: AsRef<Path>, S: AsRef<str>, A: AsRef<[S]>>(
                    &self,
                    opts: &EncryptOpts<P, S, A, impl Progress>,
                ) -> Result<Option<String>>;
            }

            pub enum Destination<'a> {
                Local(local::LocalOpts<'a>),
                Sftp(sftp::SftpOpts<'a>),
                S3(s3::S3Opts<'a>),
            }
            impl Encrypt for Destination<'_> {}
            </script></code></pre>
          </section>

          <section>
            <h3>Python bindings (sett-rs)</h3>
            <pre><code data-trim data-noescape class="rust"><script type="text/template">
              #[pyfunction]
              fn encrypt(
                  _py: Python,
                  opts: EncryptOpts,
                  dest: Destination,
                  progress: Option<Callable<'_>>,
                  two_factor_callback: Option<Callable<'_>>,
              ) -> PyResult<Option<String>> {}

              #[pymodule]
              fn sett_rs(_py: Python, m: &PyModule) -> PyResult<()> {
                  pyo3_log::init();
                  m.add_function(wrap_pyfunction!(encrypt, m)?)?;
                  Ok(())
              }

              // from sett_rs import encrypt, EncryptOpts, Destination
              // encrypt(EncryptOpts(...), Destination(...), None, None)
            </script></code></pre>
          </section>
        </section>

        <section>
          <section>
            <h2>Maintenance</h2>
            <a href="https://youtu.be/r35cBkPRNMI"
              >Living with Rust Long-Term - Jon Gjengset</a
            >
            <img src="img/rusty.jpg" height="400px" />
          </section>
          <section>
            <h3>CI/CD 🦾</h3>
            <div class="r-stack">
              <a
                class="fragment"
                href="https://gitlab.com/biomedit/sett-rs/-/blob/main/.gitlab-ci.yml"
              >
                <img src="img/cicd_pipeline.png" height="300px" />
              </a>
              <a class="fragment" href="https://pypi.org/project/sett-rs/">
                <img src="img/sett-rs_pypi.png" height="500px" />
              </a>
              <a
                class="fragment"
                href="https://gitlab.com/biomedit/sett-rs/-/packages/"
              >
                <img src="img/sett_package_registry.png" height="470px" />
              </a>
            </div>
          </section>

          <section data-background-size="400px">
            <h3>Releases 🚀</h3>
            <a href="https://git-cliff.org/"
              ><img src="img/git-cliff.png" height="150px"
            /></a>
            <blockquote cite="https://git-cliff.org/">
              <a href="https://git-cliff.org/">git-cliff</a> can generate
              changelog files from the Git history by utilizing conventional
              commits as well as regex-powered custom parsers. The changelog
              template can be customized with a configuration file to match the
              desired format.
            </blockquote>
            <p class="fragment">
              Separate releases for <strong>sett</strong> and
              <strong>sett-rs</strong> crates automated with a simple
              <a
                href="https://gitlab.com/biomedit/sett-rs/-/blob/main/dev-tools/bumpversion.sh"
                >script</a
              >.
            </p>
          </section>

          <section>
            <h3>Stay up-to-date</h3>
            <p>
              <a href="https://github.com/renovatebot/renovate">Renovate bot</a>
            </p>
            <div class="r-stack">
              <a
                class="fragment"
                href="https://gitlab.com/biomedit/sett-rs/-/merge_requests/58"
              >
                <img src="img/v1.68.0_mr.png" height="420px" />
              </a>
              <a
                class="fragment"
                href="https://gitlab.com/biomedit/sett-rs/-/issues/40"
              >
                <img src="img/dependency_dashboard.png" height="400px" />
              </a>
            </div>
          </section>
        </section>

        <section>
          <section>
            <h2>What's next?</h2>
            <a href="https://xkcd.com/338/">
              <img src="img/338_future.png" height="210px" />
            </a>
          </section>

          <section>
            <h3>Performance improvements</h3>
            <div class="container">
              <div class="col">
                <ul>
                  <li>
                    Multithreaded compression (<a
                      href="https://github.com/sstadick/gzp/"
                      >gzp</a
                    >)
                  </li>
                  <li class="fragment">
                    Streaming support for <strong>S3</strong>
                  </li>
                </ul>
              </div>
              <div class="col">
                <p>
                  <img
                    src="img/flamegraph_thread_sftp.svg"
                    style="height: 500px"
                  />
                </p>
              </div>
            </div>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - Mention how easy implementable the streaming was in **Rust**. We still have to
                find out how in **Python**... 😅
              </textarea>
            </aside>
          </section>

          <section>
            <h3>Packaging and distribution 📦</h3>
            <ul>
              <li>Easy (native) installer</li>
              <li>Self updater</li>
              <li>
                <strong>CLI</strong> and <strong>GUI</strong> in one executable
              </li>
            </ul>
          </section>

          <section>
            <h3>GUI</h3>
            <div class="r-stack">
              <a href="https://www.areweguiyet.com/">
                <img
                  class="fragment semi-fade-out"
                  data-fragment-index="0"
                  src="img/are_we_gui_yet.png"
                  width="500px"
                />
              </a>
              <div class="fragment" data-fragment-index="0">
                <p>
                  <a href="https://tauri.app">Tauri</a> +
                  <a href="https://yew.rs">Yew</a>?
                </p>
                <p>
                  <img
                    src="img/tauri_yew_app_example.png"
                    style="height: 300px"
                  />
                </p>
              </div>
            </div>
            <p class="fragment">
              <img src="img/wasm-ferris.png" height="50px" />
              sett in a browser with WebAssembly?
            </p>
            <p></p>
          </section>
        </section>

        <section data-markdown>
          <textarea data-template>
          ## Summary

          - Python
            - Easy initial implementation
            - Difficult to maintain
            - Complicated distribution
          - Rust
            - Steep learning curve
            - Early bug detection
            - Easy to maintain and distribute
          - Good CI/CD and dependency update automation saves time and effort in the long-term.
          </textarea>
        </section>

        <section
          data-background-image="img/oeschinensee.jpg"
          data-background-size="cover"
        >
          <p class="r-stretch"></p>
          <p style="color: white">That's all for now 🏖️</p>
          <p style="font-size: 50%; color: white">
            <em>Oeschinensee, 2021</em>
          </p>
        </section>

        <section>
          <h3>References</h3>
          <ul>
            <li>
              <a href="https://gitlab.com/biomedit/sett-rs">sett-rs 🦀</a>
            </li>
            <li><a href="https://gitlab.com/biomedit/sett">sett 🐍</a></li>
            <li><a href="https://biomedit.ch">biomedit.ch</a></li>
          </ul>
        </section>
      </div>
    </div>

    <script src="dist/reveal.js"></script>
    <script src="plugin/notes/notes.js"></script>
    <script src="plugin/markdown/markdown.js"></script>
    <script src="plugin/highlight/highlight.js"></script>
    <script>
      Reveal.initialize({
        hash: true,
        slideNumber: "c/t",
        plugins: [RevealMarkdown, RevealHighlight, RevealNotes],
      });
    </script>
  </body>
</html>
